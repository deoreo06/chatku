package id.edo.chatku.model;

public class ModelContact {
    String user_id, user_key, user_name, user_username, user_email, user_photo, user_online, user_location, user_flag, user_distance;

    public ModelContact() {

    }

    public ModelContact(String user_id, String user_key, String user_name, String user_username, String user_email, String user_photo, String user_online, String user_location, String user_flag, String user_distance) {
        this.user_id = user_id;
        this.user_key = user_key;
        this.user_name = user_name;
        this.user_username = user_username;
        this.user_email = user_email;
        this.user_photo = user_photo;
        this.user_online = user_online;
        this.user_location = user_location;
        this.user_flag = user_flag;
        this.user_distance = user_distance;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_key() {
        return user_key;
    }

    public void setUser_key(String user_key) {
        this.user_key = user_key;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_username() {
        return user_username;
    }

    public void setUser_username(String user_username) {
        this.user_username = user_username;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getUser_online() {
        return user_online;
    }

    public void setUser_online(String user_online) {
        this.user_online = user_online;
    }

    public String getUser_location() {
        return user_location;
    }

    public void setUser_location(String user_location) {
        this.user_location = user_location;
    }

    public String getUser_flag() {
        return user_flag;
    }

    public void setUser_flag(String user_flag) {
        this.user_flag = user_flag;
    }

    public String getUser_distance() {
        return user_distance;
    }

    public void setUser_distance(String user_distance) {
        this.user_distance = user_distance;
    }
}

