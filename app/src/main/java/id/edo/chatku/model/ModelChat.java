package id.edo.chatku.model;

public class ModelChat {
    private String message_id,from_user_id, to_user_id, from_user_name,to_user_name, content, status, msg_status, timestamp;

    public ModelChat(){}

    public ModelChat(String message_id, String from_user_id, String to_user_id, String from_user_name, String to_user_name, String content, String status, String msg_status, String timestamp) {
        this.message_id = message_id;
        this.from_user_id = from_user_id;
        this.to_user_id = to_user_id;
        this.from_user_name = from_user_name;
        this.to_user_name = to_user_name;
        this.content = content;
        this.status = status;
        this.msg_status = msg_status;
        this.timestamp = timestamp;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getFrom_user_name() {
        return from_user_name;
    }

    public void setFrom_user_name(String from_user_name) {
        this.from_user_name = from_user_name;
    }

    public String getTo_user_name() {
        return to_user_name;
    }

    public void setTo_user_name(String to_user_name) {
        this.to_user_name = to_user_name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg_status() {
        return msg_status;
    }

    public void setMsg_status(String msg_status) {
        this.msg_status = msg_status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
