package id.edo.chatku.app;

public class AppAddress {

    public static final String SERVER = "http://167.99.66.123:2727/";
    public static final String LOGIN = SERVER + "auth/login";
    public static final String CONVERSATION = SERVER + "api/list_conversation?page=1";
    public static final String CONTACT =SERVER + "api/list_user?page=1";
}
