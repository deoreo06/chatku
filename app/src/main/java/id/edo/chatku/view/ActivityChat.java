package id.edo.chatku.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.edo.chatku.R;
import id.edo.chatku.app.AppTemp;
import id.edo.chatku.model.ModelChat;
import id.edo.chatku.presenter.AdapterChat;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static id.edo.chatku.app.AppAddress.SERVER;

public class ActivityChat extends AppCompatActivity {

    private Activity _activity;
    private RecyclerView.LayoutManager _layoutManager;
    private ArrayList<ModelChat> _modelItems = new ArrayList<>();
    @BindView(R.id.editTextChat)
    EditText _etChat;
    @BindView(R.id.textviewNameContact)
    TextView _tvNameContact;
    @BindView(R.id.textviewEmailContact)
    TextView _tvEmailContact;
    @BindView(R.id.btnSend)
    Button _btnSend;
    @BindView(R.id.screenChat)
    RecyclerView _screenChat;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(SERVER);
        } catch (URISyntaxException ignored) {}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        _activity = this;
        initRecyclerView();
        _tvNameContact.setText(AppTemp.modelContact.getUser_name());
        _tvEmailContact.setText(AppTemp.modelContact.getUser_email());
        mSocket.on("received message", onNewMessage);
        mSocket.connect();
    }

    private void initRecyclerView() {
        try {

            _screenChat.setHasFixedSize(true);
            _layoutManager = new LinearLayoutManager(this);
            _screenChat.setLayoutManager(_layoutManager);
        }catch (Exception ignored){}
    }

    @OnClick({R.id.btnSend})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                Log.d("Chat", "OKE");
                attemptSend();
                break;

        }
    }

    private void attemptSend() {
        String message = _etChat.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            return;
        }

        _etChat.setText("");
        addMessage(message);
        mSocket.emit("send message", onNewMessage);
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            _activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {

                        JSONObject gtfwRequest = new JSONObject();
                        JSONObject dataObject = new JSONObject();
                        JSONObject jsonObject = new JSONObject();

                        jsonObject.put("user_id", AppTemp.modelContact.getUser_id());
                        jsonObject.put("message",  _etChat.getText().toString().trim());

                        dataObject.put("data", jsonObject);
                        gtfwRequest.put("gtfwRequest", dataObject);
                        Log.d("output", gtfwRequest.toString());



                    } catch (JSONException e) {
                        Log.d("Emit", e.getMessage());
                    }

                }
            });
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.disconnect();
        mSocket.off("received message", onNewMessage);
    }

    private void addMessage(String message) {

        ModelChat modelChat = new ModelChat();
        modelChat.setFrom_user_name(AppTemp.modelUser.getName());
        modelChat.setContent(message);
        _modelItems.add(modelChat);

        AdapterChat adapter = new AdapterChat(_modelItems, _activity);

        _screenChat.setAdapter(adapter);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
