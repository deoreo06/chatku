package id.edo.chatku.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import id.edo.chatku.R;
import id.edo.chatku.model.ModelUser;
import id.edo.chatku.presenter.RequestServer;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivityLogin extends AppCompatActivity {

    private Activity _activity;
    private ModelUser _modelUser;
    private String _strEmail, _strPassword;
    @BindView(R.id.editTextEmail) EditText _etEmail;
    @BindView(R.id.editTextPassword) EditText _etPassword;
    @BindView(R.id.buttonLogin) Button _btnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        _activity = this;


    }

    @OnClick({R.id.buttonLogin})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonLogin:
                _strEmail = _etEmail.getText().toString();
                _strPassword = _etPassword.getText().toString();

                if(_strEmail.isEmpty() || _strPassword.isEmpty())
                {
                    Toast.makeText(_activity,"Mohon Isi Email dan Password Anda!", Toast.LENGTH_LONG ).show();
                }
                else {
                    RequestServer.getInstance(this, _activity).Login(_strEmail, _strPassword);
                }
                break;

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
