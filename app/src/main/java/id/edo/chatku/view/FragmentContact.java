package id.edo.chatku.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.edo.chatku.R;
import id.edo.chatku.app.AppTemp;
import id.edo.chatku.model.ModelContact;
import id.edo.chatku.presenter.RequestServer;

public class FragmentContact extends Fragment {

    private Activity _activity;
    private RecyclerView.LayoutManager _layoutManager;
    private ArrayList<ModelContact> _modelItems = new ArrayList<>();
    @BindView(R.id.recycler_view)
    RecyclerView _recyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout _swipeRefresh;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_conversation, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _activity = getActivity();
        initRecyclerView();
        GetConversations();
    }


    private void initRecyclerView() {
        try {
            _swipeRefresh.setRefreshing(true);
            _recyclerView.setHasFixedSize(true);
            _layoutManager = new LinearLayoutManager(_activity);
            _recyclerView.setLayoutManager(_layoutManager);
            _swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    GetConversations();
                }
            });
        }catch (Exception ignored){}
    }

    private void GetConversations() {
        RequestServer.getInstance(getActivity(), _activity).ListItem(
                AppTemp.modelUser.getToken(),
                _modelItems,
                _recyclerView,
                true
                );
        _swipeRefresh.setRefreshing(false);
        _swipeRefresh.setEnabled(true);

    }


}
