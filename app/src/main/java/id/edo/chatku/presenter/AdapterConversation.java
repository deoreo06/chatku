package id.edo.chatku.presenter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.edo.chatku.R;
import id.edo.chatku.app.AppTemp;
import id.edo.chatku.model.ModelContact;
import id.edo.chatku.view.ActivityChat;

public class AdapterConversation extends RecyclerView.Adapter<AdapterConversation.ViewHolder> {

    private Activity _activity;
    private ArrayList<ModelContact> modelContacts = new ArrayList<>();

    public AdapterConversation(ArrayList<ModelContact> modelContacts, Activity activity) {
        super();
        this._activity = activity;
        this.modelContacts = modelContacts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contact, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ModelContact items = modelContacts.get(position);

        final String id = items.getUser_id();
        final String nama = items.getUser_name();
        final String photo = items.getUser_photo();
/*
        if (items.getUser_photo().isEmpty()) {
            holder._ivPlaceholder.setVisibility(VISIBLE);
            holder._avatar.setVisibility(View.INVISIBLE);
        } else if (items.getUser_photo().contains("http")) {
            holder._ivPlaceholder.setVisibility(View.INVISIBLE);
            holder._avatar.setVisibility(VISIBLE);
            Glide
                    .with(activity)
                    .load(photo)
                    .centerCrop()
                    .crossFade()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder._avatar);
        } else {
            byte[] imageByteArray = Base64.decode(items.getUser_photo(), Base64.NO_WRAP);
            holder._ivPlaceholder.setVisibility(View.INVISIBLE);
            holder._avatar.setVisibility(VISIBLE);
            Glide
                    .with(activity)
                    .load(imageByteArray)
                    .asBitmap()
                    .centerCrop()
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder._avatar);
        }*/
        holder._tvNama.setText(nama);
        holder.listItem.setTag(position);
    }

    @Override
    public int getItemCount() {
        return modelContacts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_placeholder)
        ImageView _ivPlaceholder;
        @BindView(R.id.avatar)
        RoundedImageView _avatar;
        @BindView(R.id.textViewName)
        TextView _tvNama;
        @BindView(R.id.btnChat)
        LinearLayout _btnChat;

        @BindView(R.id.row_contact)
        LinearLayout listItem;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.btnChat})
        public void onClick(View v) {
            try {
                int i = getAdapterPosition();
                ModelContact item = modelContacts.get(i);
                AppTemp.modelContact.setUser_id(item.getUser_id());
                AppTemp.modelContact.setUser_name(item.getUser_name());
                AppTemp.modelContact.setUser_email(item.getUser_email());
                AppTemp.modelContact.setUser_photo(item.getUser_photo());

                Intent intent = new Intent(_activity, ActivityChat.class);
                _activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                _activity.startActivity(intent);
                _activity.finish();
            } catch (Exception e) {
                Log.d("AdapterConversation", e.getMessage());

            }


            switch (v.getId()) {
                case R.id.btnChat:
                    Log.d("AdapterConversation", "btnChat");
                    break;

            }


        }


    }
}
