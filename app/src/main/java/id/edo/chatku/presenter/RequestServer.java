package id.edo.chatku.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import id.edo.chatku.R;
import id.edo.chatku.app.AppTemp;
import id.edo.chatku.model.ModelContact;
import id.edo.chatku.model.ModelUser;
import id.edo.chatku.view.ActivityHome;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static id.edo.chatku.app.AppAddress.CONTACT;
import static id.edo.chatku.app.AppAddress.CONVERSATION;
import static id.edo.chatku.app.AppAddress.LOGIN;

public class RequestServer {

    private Context _context;
    private Activity _activity;
    private static RequestServer _instance = null;
    private RequestQueue _queue;
    public static RequestServer getInstance(Context context, Activity activity) {
        if (_instance == null)
            _instance = new RequestServer(context, activity);
        return _instance;
    }

    public RequestServer(Context context, Activity activity) {
        this._context = context;
        this._activity = activity;
         _queue = Volley.newRequestQueue(_context);
    }

    public void Login(final String username, final String password){

        StringRequest postRequest = new StringRequest(POST, LOGIN,
                new Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            JSONObject gtfwResultObject = responseObject.getJSONObject("gtfwResult");
                            JSONObject dataObject = gtfwResultObject.getJSONObject("data");
                            String user_id = dataObject.getString("user_id");
                            String user_name = dataObject.getString("user_name");
                            String user_username = dataObject.getString("user_username");
                            String user_phone = dataObject.getString("user_phone");
                            String user_email = dataObject.getString("user_email");
                            String user_role = dataObject.getString("role");
                            String token = dataObject.getString("token");
                            AppTemp.modelUser = new ModelUser(user_id,user_name,user_username, user_phone, user_email, user_role, token);
                            Toast.makeText(_context,"Selamat Datang "+ user_name, Toast.LENGTH_LONG ).show();


                            Intent intent = new Intent(_activity, ActivityHome.class);
                            _activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            _activity.startActivity(intent);
                            _activity.finish();
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                        Toast.makeText(_activity,"Maaf, Email atau Password Anda Salah!", Toast.LENGTH_LONG ).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);

                return params;
            }
        };

        _queue.add(postRequest);
    }

    public void ListItem(final String token, final ArrayList<ModelContact> modelItems, final RecyclerView recyclerView, boolean isContact){

        String URL;
        if(isContact) URL = CONTACT;
        else URL = CONVERSATION;

        StringRequest postRequest = new StringRequest(GET, URL,
                new Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            JSONObject gtfwResultObject = responseObject.getJSONObject("gtfwResult");
                            JSONObject dataObject = gtfwResultObject.getJSONObject("data");
                            JSONArray listUser = dataObject.getJSONArray("list_user");
                            for(int i=0;i<listUser.length();i++){
                                ModelContact modelContact = new ModelContact();
                                JSONObject contact = listUser.getJSONObject(i);
                                modelContact.setUser_id(contact.getString("user_id"));
                                modelContact.setUser_name(contact.getString("user_name"));
                                modelContact.setUser_photo(contact.getString("user_photo"));
                                modelContact.setUser_email(contact.getString("user_email"));
                                modelContact.setUser_photo(contact.getString("user_location"));
                                modelContact.setUser_distance(contact.getString("user_distance"));
                                modelItems.add(modelContact);
                            }
                            AdapterConversation adapter = new AdapterConversation(modelItems, _activity);
                            recyclerView.setAdapter(adapter);
                            Log.d("RequestServer", response);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", "error");
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", token);

                return params;
            }

        };

        _queue.add(postRequest);
    }


}
