package id.edo.chatku.presenter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.edo.chatku.R;
import id.edo.chatku.model.ModelChat;

public class AdapterChat extends RecyclerView.Adapter<AdapterChat.ViewHolder> {

    private Activity _activity;
    private ArrayList<ModelChat> modelChats = new ArrayList<>();

    public AdapterChat(ArrayList<ModelChat> modelChats, Activity activity) {
        super();
        this._activity = activity;
        this.modelChats = modelChats;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_receive, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ModelChat items = modelChats.get(position);

        final String id = items.getMessage_id();
        final String from_name = items.getFrom_user_name();
        final String to_name = items.getTo_user_name();
        final String from_id = items.getFrom_user_id();
        final String to_id = items.getTo_user_id();
        final String message = items.getContent();
        final String status = items.getStatus();

        holder._tvName.setText(from_name);
        holder._tvContent.setText(message);
        holder.listItem.setTag(position);
    }

    @Override
    public int getItemCount() {
        return modelChats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textviewName)
        TextView _tvName;
        @BindView(R.id.textviewContent)
        TextView _tvContent;
        @BindView(R.id.row_chat_receive)
        RelativeLayout listItem;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }



    }
}
